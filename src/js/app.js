(function ($) {
	var investState = {
		sumInv: {
			required: true,
			label: "Сумма инвестиции",
			value: 5000,
			min: 100,
			max: 200000,
			validationErrors: {
				min: "Минимальная сумма инвестиции <span>$ {%1%}</span>"
			}
		},
		mult: {
			required: true,
			label: "Мультипликатор",
			showRange: false,
			value: 40,
			min: 1,
			max: 40,
			validationErrors: {
				all: "Неверное значение мультипликатора"
			}
		},
		amount: 200000,
		showLimits: false,
		limits: {
			required: true,
			label: "Ограничения в",
			value: 1,
			types: ['%', '$']
		},
		takeProfit: {
			required: false,
			label: "Прибыль",
			isLimit: true,
			enable: false,
			value: 60000,
			percentValue: 30,
			minPercentageOfAmount: 10,
			validationErrors: {
				min: "Не может быть меньше <span>{%1%}{%2%}</span>"
			}
		},
		stopLoss: {
			required: false,
			label: "Убыток",
			isLimit: true,
			enable: false,
			value: 60000,
			percentValue: 30,
			minPercentageOfAmount: 10,
			maxPercentageOfAmount: 100,
			validationErrors: {
				min: "Не может быть меньше <span>{%1%}{%2%}</span>",
				max: "Не может быть больше <span>{%1%}{%2%}</span>"
			}
		}
	}

	
	var InvestModel = Backbone.Model.extend({
		
		url: '/serverurl',
		
		send: function(direction){
			if(!this.isValid()) {
				return false;
			}
			
			var data = "";
			data += "sumInv=" + this.get('sumInv').value + ";";
			data += "mult=" + this.get('mult').value + ";";
			if (this.isEnableField('takeProfit')) {
				data += "takeProfit=" + this.get('takeProfit').value + ";";
			}
			if (this.isEnableField('stopLoss')) {
				data += "stopLoss=" + this.get('stopLoss').value + ";";
			}
			data += "direction=" + direction + ";";
			
			console.log(data);
			
			return this.fetch({
				contentType: 'application/json',
				type: 'POST',
				cache: false,
				data: data,
				url: this.url
			});
		},
		
		recalcAmount: function() {
			var newAmount = this.get('sumInv').value * this.get('mult').value,
				currentTakeProfit = this.get('takeProfit'),
				currentStopLoss = this.get('stopLoss');
			if (this.get('limits').value) {
				currentTakeProfit.percentValue = Math.floor(currentTakeProfit.value / newAmount * 100);
				currentStopLoss.percentValue = Math.floor(currentStopLoss.value / newAmount * 100);
			} else {
				if (Math.round(currentTakeProfit.value / newAmount * 100) != currentTakeProfit.percentValue) {
					currentTakeProfit.value = Math.round(newAmount * (currentTakeProfit.percentValue / 100));	
				}
				if (Math.round(currentStopLoss.value / newAmount * 100) != currentStopLoss.percentValue) {
					currentStopLoss.value = Math.round(newAmount * (currentStopLoss.percentValue / 100));	
				}
			}
			this.set({amount: newAmount});
		},

		toggleLimits: function() {
			this.set({showLimits: !this.get('showLimits')}, {validate: true});
		},
		
		showMultRange: function() {
			this.get('mult').showRange = true;
		},
		
		hideMultRange: function() {
			this.get('mult').showRange = false;
		},

		setLimits: function(limitsValue) {
			if (limitsValue) {
				this.set({limits: {value: 1}});
			} else {
				this.set({limits: {value: 0}});
			}
		},

		getMinValue: function(attr) {
			return this.get(attr).hasOwnProperty('min') ? 
								this.get(attr).min : this.get(attr).hasOwnProperty('minPercentageOfAmount') ?
																	this.get('amount') * (this.get(attr).minPercentageOfAmount / 100) : 0;													
		},

		updateValue: function(attr, value) {
			var value = isNaN(value) ? this.getMinValue(attr) : value,
				needRecalcAmount = attr === 'sumInv' || attr === 'mult';
			if (needRecalcAmount && value < this.get(attr).min) {
				value = this.get(attr).min;
			} else if (needRecalcAmount && value > this.get(attr).max) {
				value = this.get(attr).max;
			}
			this.get(attr).value = value;
			if (this.get(attr).hasOwnProperty('percentValue')) {
				this.get(attr).percentValue = Math.floor(value / this.get('amount') * 100);
			}
			if (needRecalcAmount) {
				this.recalcAmount();
			}
			this.trigger("change");
			this.isValid();
		},

		updatePercentValue: function(attr, value) {
			var value = isNaN(value) ? this.getMinValue(attr) : value;
			this.get(attr).percentValue = value;
			if (Math.round(this.get(attr).value / this.get('amount') * 100) != value) {
				this.get(attr).value = Math.round(this.get('amount') * (value / 100));	
			}
			this.trigger("change");
			this.isValid();
		},
		
		stepValue: function(attr, isIncrement) {
			var step = isIncrement ? 1 : -1;
			if (!this.isEnableField(attr)) {
				return;
			}
			if (this.get('limits').value) {
				this.updateValue(attr, this.get(attr).value + step);
			} else {
				this.updatePercentValue(attr, this.get(attr).percentValue + step);
			}
		},
		
		switchEnable: function(attr, value) {
			var properties = this.get(attr),
				currentAmount = this.get('amount');
			if (properties.hasOwnProperty('enable')) {
				properties.enable = value;
			}
			if (Math.round(properties.value / this.get('amount') * 100) != properties.percentValue) {
				properties.value = Math.round(currentAmount * (properties.percentValue / 100));	
			}		
			this.trigger("change");
			this.isValid();
		},
		
		isEnableField: function(fieldId) {
			var properties = this.get(fieldId);
			return properties.hasOwnProperty('enable') ? properties.enable : true;
		},

		validate: function(attrs, options) {
			var _this = this,
				limitType = attrs.hasOwnProperty('limits') && attrs.limits.hasOwnProperty('value') ? attrs.limits.value : 0;
			
			_this.errors = {};

			_.each(attrs, function(properties, attr){
				if (typeof(properties) !== 'object') {
					return;
				}
				var isEnable = _this.isEnableField(attr);
				if (isEnable && properties.hasOwnProperty('min') && properties.hasOwnProperty('value') && properties.value < properties.min ) {
					_this.errors[attr] = _this.getValidationError(properties, [properties.min], 'min');
				} else if (isEnable && properties.hasOwnProperty('max') && properties.hasOwnProperty('value') && properties.value > properties.max ) {
					_this.errors[attr] = _this.getValidationError(properties, [properties.max], 'max');
				} else if (isEnable &&
						   properties.hasOwnProperty('minPercentageOfAmount') && 
						   properties.hasOwnProperty('value') && 
						   properties.hasOwnProperty('isLimit') &&
						   attrs.hasOwnProperty('amount') && 
						   (limitType && properties.value < attrs.amount * (properties.minPercentageOfAmount / 100) ||
						   !limitType && properties.percentValue < properties.minPercentageOfAmount)) {
					if (limitType) {
						_this.errors[attr] = _this.getValidationError(properties, [attrs.limits.types[limitType]+' ', attrs.amount * (properties.minPercentageOfAmount / 100)], 'min');
					} else {
						_this.errors[attr] = _this.getValidationError(properties, [properties.minPercentageOfAmount, attrs.limits.types[limitType]], 'min');
					}
				} else if (isEnable &&
						   properties.hasOwnProperty('maxPercentageOfAmount') && 
						   properties.hasOwnProperty('value') && 
						   properties.hasOwnProperty('isLimit') &&
						   attrs.hasOwnProperty('amount') &&  
						   (limitType && properties.value > attrs.amount * (properties.maxPercentageOfAmount / 100) ||
						   !limitType && properties.percentValue > properties.maxPercentageOfAmount)) {
					if (limitType) {
						_this.errors[attr] = _this.getValidationError(properties, [attrs.limits.types[limitType]+' ', attrs.amount * (properties.maxPercentageOfAmount / 100)], 'max');
					} else {
						_this.errors[attr] = _this.getValidationError(properties, [properties.maxPercentageOfAmount, attrs.limits.types[limitType]], 'max');
					}
				}
			});
			if (!_.isEmpty(_this.errors)) {
				return _this.errors;
			}
		},

		getValidationError: function (properties, values, errorType) {
			var message,
				defaultMessage = "Ошибка валидации";
			try {
				message = properties.validationErrors[errorType] ? properties.validationErrors[errorType] : properties.validationErrors['all'];
				for (var i = 0; i < values.length; i++) {
					message = message.replace("{%"+(i+1)+"%}", (values[i]+"").replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 "));
				}
			} catch (err) {
				console.warn("Exception in getValidationError");
				console.warn(arguments);
			}
			return message || defaultMessage;
		}
	});


	var InvestAppView = Backbone.View.extend({
		el: $("#investForm"),
		template: $("#investFormTemplate").html(),

		events: {
			"click" 						: "toggleMultRange",
			"click .c-invest_toggle"		: "toggleLimits",
			"click .c-invest_control"		: "changeStepValue",
			"blur input"   					: "changeValue",
			"change input[type='radio']"   	: "changeValue",
			"mouseup input[type='range']"   : "changeValue",
			"change input[type='checkbox']" : "switchEnable",
			"click .c-button"				: "sendData"
		},

		initialize: function () {
			this.model = new InvestModel(investState);
			this.listenTo(this.model, "change", this.render);
			this.model.on("invalid", this.invalid);
			this.render();
		},

		render: function () {
			var tmpl = _.template(this.template);
			this.$el.html(tmpl(this.model.toJSON()));
		},
		
		toggleMultRange: function(e) {
			var current = $(e.target),
				rangeInput = this.$('.c-invest_range'),
				isMultInput = current.attr('name') && current.attr('name') === 'mult' && current.hasClass('c-invest_input');
			if (current != rangeInput && !current.hasClass('c-invest_range') && !current.parent().hasClass('c-invest_range') && !isMultInput && rangeInput.hasClass('_show')) {
				rangeInput.removeClass('_show');
				this.model.hideMultRange();
			} else if (isMultInput && !rangeInput.hasClass('_show')) {
				rangeInput.addClass('_show');
				this.model.showMultRange();
			}
		},
		
		toggleLimits: function() {
			this.model.toggleLimits();
		},

		invalid: function(model, errors) {
			_.each(errors, function(message, fieldId){
				if (!model.isEnableField(fieldId)) {
					return;
				}
				this.$('#'+fieldId).addClass('_validation-error').find('.c-invest_validation').addClass('_error').html(message);
			});
		},

		changeValue: function(e) {
			var input = $(e.currentTarget);
			if (input.hasClass('_limit-depends') && input.hasClass('_percent')) {
				this.model.updatePercentValue(input.attr("name"), parseInt(input.val(), 10));
			} else {
				this.model.updateValue(input.attr("name"), parseInt(input.val(), 10));
			}
		},

		changeStepValue: function(e) {
			var control = $(e.target),
				target = control.parent().parent().attr('id');
			this.model.stepValue(target, control.hasClass('_increment'));
		},
		
		switchEnable: function(e) {
			var input = $(e.currentTarget);
			this.model.switchEnable(input.attr("name").replace('_switch', ''), input.is(':checked'));
		},
		
		sendData: function(e) {
			var button = $(e.target);
			this.model.send(button.attr('id'));
		}
	});

	var application = new InvestAppView();
} (jQuery));